use std::fs::File;
use std::io::Read;
use std::time::SystemTime;

static FILEPATH: &str = "/home/vh/tmp/CentOS-7-x86_64-Minimal-1503-01.iso";

fn main() {
    let time = SystemTime::now();
    let mut counter: usize = 0;
    let mut file = File::open(FILEPATH).unwrap();
    let mut buf = [0u8; 16*1024];
    loop {
        match file.read(&mut buf) {
            Ok(n) => {
                if n == 0 {
                    // eof
                    break;
                } else {
                    counter += n;
                }
            },
            Err(_) => panic!("Error while reading file")
        }
    }
    println!("Read {} bytes in {} µs", counter,
             time.elapsed().unwrap().as_micros());
}
